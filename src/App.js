import logo from './assets/images/logo.svg';
import './assets/css/App.css';

// Importar componentes

import Formulario from './components/Formulario';
import HeaderPage from './components/HeaderPage';
import Separador from './components/Separador';
import Tarjeta001 from './components/Tarjeta001';


function App() {
  return (
    <div className="App">
      <header className="">
         <HeaderPage />
      </header>
      <body className="contenedorPrincipal">
        <section className="seccion-formulario">
            <Formulario />
        </section>
        <section className="favorites">
          <Separador />
          <Tarjeta001 /> 
          <Tarjeta001 /> 
          <Tarjeta001 /> 
          <Tarjeta001 /> 
        </section>
      </body>
      
    </div>
  );
}

export default App;
